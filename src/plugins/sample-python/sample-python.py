# From code by James Livingston

from gi.repository import GObject
from gi.repository import Peas
from gi.repository import Totem

class SamplePython(GObject.Object, Peas.Activatable):
	__gtype_name__ = 'SamplePython'

	object = GObject.property(type = GObject.Object)

	def do_activate(self):
		print "Activating sample Python plugin"
		self.object.action_fullscreen_toggle()
	
	def do_deactivate(self):
		print "Deactivating sample Python plugin"
